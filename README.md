# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://SergejMunda@bitbucket.org/SergejMunda/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/SergejMunda/stroboskop/commits/3f4ac5a2d74a2c6233de2ccf2479a1fa866e5adf

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/SergejMunda/stroboskop/commits/01b99afba0ec7efdda23a71e75007bd09bd2fede

Naloga 6.3.2:
https://bitbucket.org/SergejMunda/stroboskop/commits/1f3b76522aab82d46d402152920585d8ba3b1d02

Naloga 6.3.3:
https://bitbucket.org/SergejMunda/stroboskop/commits/616e20cbb05a4630cb0df01b563aaa41651280d0

Naloga 6.3.4:
https://bitbucket.org/SergejMunda/stroboskop/commits/515e159a82772f26aa384dcc5658cef6d183ad20

Naloga 6.3.5:

git checkout master
git merge izgled
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/SergejMunda/stroboskop/commits/22b44d2b468093c0db54293cc82d7dcdba0b689c

Naloga 6.4.2:
https://bitbucket.org/SergejMunda/stroboskop/commits/a7c1bb2ff1809ec8af204b5c2b2513fa7049871b

Naloga 6.4.3:
https://bitbucket.org/SergejMunda/stroboskop/commits/08bb030566aa6895e40f97b95334c3e391db1998

Naloga 6.4.4:
https://bitbucket.org/SergejMunda/stroboskop/commits/aebf2d204c0f402fd67ea0d919c1414b2b4faa47